# NOS-scraper

Het doel van dit R-project is om alle tekst van de NOS-website te scrapen in een bepaalde periode.

Op dit moment worden standaard alle categorieën gescrapt, maar je kunt aangeven
als je alleen uit een specifieke categorie nieuwsberichten wilt hebben.

Standaard worden de berichten van vandaag opgehaald, maar de functie kent een 
*datum_start* en een *datum_eind*, die beiden binnen de te downloaden range vallen.

## To do
Categorie nog scrapen en toevoegen als categorie.